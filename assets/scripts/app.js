const defaultResult = 0;
let currentResult = defaultResult;
let logEntries = []; //plurals name

// get input from input field
function getUserNumberInput() {
  return parseInt(userInput.value);
}

// generates and writes calculation log
function createAndWriteOutput(operator, resultBeforeCalc, calcNumber) {
  const calcDescription = `${resultBeforeCalc} ${operator} ${calcNumber}`;
  outputResult(currentResult, calcDescription); // from vendor file
}

function writeToLog(
  operationIdentifier,
  prevResult,
  operationNumber,
  newResult
) {
  const logEntry = {
    operation: operationIdentifier,
    prevResult: prevResult,
    number: operationNumber,
    result: newResult
  }; //creating objects
  logEntries.push(logEntry); // calling operation
  console.log(logEntries);
}

function add() {
  const enterNum = getUserNumberInput();
  //const calcDescription = `${currentResult} + ${enterNum}`;
  const initialResult = currentResult;
  currentResult += enterNum;
  //outputResult(currentResult, calcDescription);
  createAndWriteOutput('+', initialResult, enterNum);
  writeToLog('ADD', initialResult, enterNum, currentResult);
}

function subtract() {
  const enterNum = getUserNumberInput();
  const initialResult = currentResult;
  currentResult = currentResult - enterNum;
  createAndWriteOutput('-', initialResult, enterNum);
  const logEntry = {
    operation: 'SUBTRACT',
    prevResult: initialResult,
    number: enterNum,
    result: currentResult
  }; //creating objects
  logEntries.push(logEntry.operation); // calling operation
  console.log(logEntries);
}

function multiply() {
  const enterNum = getUserNumberInput();
  const initialResult = currentResult;
  currentResult *= enterNum;
  createAndWriteOutput('*', initialResult, enterNum);
  writeToLog('MULTIPLY', initialResult, enterNum, currentResult);
}

function divide() {
  const enterNum = getUserNumberInput();
  const initialResult = currentResult;
  currentResult /= enterNum;
  createAndWriteOutput('/', initialResult, enterNum);
  writeToLog('DIVIDE', initialResult, enterNum, currentResult);
}

addBtn.addEventListener('click', add);
subtractBtn.addEventListener('click', subtract);
multiplyBtn.addEventListener('click', multiply);
divideBtn.addEventListener('click', divide);
